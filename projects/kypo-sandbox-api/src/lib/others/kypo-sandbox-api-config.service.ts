import { Injectable } from '@angular/core';
import { KypoSandboxConfig } from './kypo-sandbox-config';

@Injectable()
export class KypoSandboxApiConfigService {
  private readonly _config: KypoSandboxConfig;

  get config(): KypoSandboxConfig {
    return this._config;
  }

  constructor(config: KypoSandboxConfig) {
    this._config = config;
  }
}
