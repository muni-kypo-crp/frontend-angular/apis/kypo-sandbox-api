export class KypoSandboxConfig {
  sandboxRestBasePath: string;

  constructor(sandboxRestBasePath: string) {
    this.sandboxRestBasePath = sandboxRestBasePath;
  }
}
