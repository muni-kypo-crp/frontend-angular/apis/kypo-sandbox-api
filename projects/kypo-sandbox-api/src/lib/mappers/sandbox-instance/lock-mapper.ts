import { Lock } from '@muni-kypo-crp/sandbox-model';
import { LockDTO } from '../../DTOs/sandbox-instance/lock-dto';

/**
 * @dynamic
 */
export class LockMapper {
  static fromDTOs(dtos: LockDTO[]): Lock[] {
    return dtos.map((dto) => LockMapper.fromDTO(dto));
  }

  static fromDTO(dto: LockDTO): Lock {
    const request = new Lock();
    request.id = dto.id;
    request.poolId = dto.pool_id;
    return request;
  }
}
