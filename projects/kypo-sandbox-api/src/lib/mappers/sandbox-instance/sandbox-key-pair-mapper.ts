import { SandboxKeyPair } from '@muni-kypo-crp/sandbox-model';
import { SandboxKeyPairDTO } from '../../DTOs/sandbox-instance/sandbox-key-pair-dto';

export class SandboxKeyPairMapper {
  static fromDTO(dto: SandboxKeyPairDTO): SandboxKeyPair {
    const request = new SandboxKeyPair();
    request.private = dto.private;
    request.public = dto.public;
    return request;
  }
}
