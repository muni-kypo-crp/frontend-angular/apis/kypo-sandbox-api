import { QuotasDTO } from './quotas-dto';

export class SandboxResourcesDTO {
  project_name: string;
  quotas: QuotasDTO;
}
