export class CloudResourceDTO {
  name: string;
  type: string;
  status: string;
}
