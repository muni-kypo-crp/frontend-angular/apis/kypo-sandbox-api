export class PoolUpdateDTO {
  comment?: string;
  visible: boolean;
  max_size: number;
  send_emails: boolean;
}
