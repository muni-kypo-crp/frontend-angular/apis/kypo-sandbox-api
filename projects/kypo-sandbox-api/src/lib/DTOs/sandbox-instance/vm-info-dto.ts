export class VMInfoDTO {
  name: string;
  id: string;
  status: string;
  creation_time: string;
  update_time: string;
  image_id: string;
  flavor_name: string;
}
