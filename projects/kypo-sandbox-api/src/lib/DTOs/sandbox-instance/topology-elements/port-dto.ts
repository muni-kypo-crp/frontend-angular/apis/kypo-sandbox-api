export class PortDTO {
  ip: string;
  mac: string;
  parent: string;
  name: string;
}
