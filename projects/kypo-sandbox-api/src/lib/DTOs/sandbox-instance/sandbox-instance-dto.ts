export class SandboxInstanceDTO {
  id: string;
  allocation_unit_id: number;
  lock_id: number;
}
