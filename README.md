# KYPO Sandbox API

This library contains Angular API services of backend [KYPO Sandbox service](https://gitlab.ics.muni.cz/kypo-crp/backend-python/kypo-sandbox-service).
It contains default implementation and mapping from DTOs to internal frontend model imported from [kypo-sandbox-model](https://gitlab.ics.muni.cz/kypo-crp/frontend-angular/models/kypo-sandbox-model).
You can override existing services by implementing the related abstract class and provide it in module.


## Prerequisites

* NPM with access to [KYPO registry](https://projects.ics.muni.cz/projects/kbase/knowledgebase/articles/153)

## Usage

To use kypo-sandbox-api in your application, follow these steps:

1. Run `npm install @muni-kypo-crp/sandbox-api`
2. Install peer dependencies
3. Create an instance of `KypoSandboxConfig`
4. Import `KypoSandboxApiModule` with config passed to `forRoot()` method

This will provide all API services with default implementation. If you want to override the default implementation, extend an API service

```
@Injectable()
export class MySandboxDefinitionApi extends SandboxDefinitionApi {
    ...
}
```

and provide your implementation in the module 

```
@NgModule({
    imports: [
       ...
    ],
    providers: [
        { provide: SandboxDefinitionApi, useClass: MySandboxDefinitionApi }
    ]
})
export class MyModule {
}

```


