18.0.0 Update to Angular 18.
16.1.0 Add training access token to pool api calls. Update sentinel version.
16.0.4 Enable change of pool size for existing pool.
16.0.3 Add option for email notifications during sandbox building.
16.0.2 Add an option to force delete a pool.
16.0.1 Fix allocation api update call.
16.0.0 Update to Angular 16, update sandbox pool and allocation units calls.
15.1.6 Fix pool update call to provide comments.
15.1.5 Adjust pool and sandbox update calls.
15.1.4 Add edition for pools, introduce comments for pools and their sandboxes.
15.1.3 Sync allocation call with new paginated results.
15.1.2 Add option for empty allocation unit array upon allocation call repsonse.
15.1.1 Enable empty allocation array upon pool overview allocation call.
15.1.0 Add option to accept empty allocation details from sandbox service's allocation call.
15.0.0 Update project to Angular 15.
14.4.5 Fix display names for ports and networks.
14.4.4 Fix sandbox allocation unit endpoint sorting by id.
14.4.3 Fix appending of string prefix in sandboxes endpoint.
14.4.2 Fix sorting of sandboxes endpoint.
14.4.1 Add force to cleanup request.
14.4.0 Change endpoints dealing with sandbox id to use uuid instead.
14.3.0 Add owner specified information to virtual image. Add query parameter 'GUI' to endpoint to filter images with GUI access.
14.2.0 Address changes in endpoints for allocation units cleanup. Add cleanup of unlocked and failed allocation units,
14.1.0 Use allocation unit id for un/locking sandbox instance. Add definition info to pool dto.
14.0.0 Update to Angular 14.
13.1.1 Add filter for images.
13.1.0 Replace openstack endpoints with terraform
13.0.0 Update to Angular 13 and CI update
12.0.5 Add support for backend sort
12.0.4 Add created by field to sandbox definition
12.0.3 Add stage to allocation units
12.0.2 New version of Sentinel, added method to request sandboxes of the specific pool.
12.0.1 Update gitlab CI
